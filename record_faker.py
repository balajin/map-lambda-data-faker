import binascii
import datetime as dt
import hashlib

from faker import Faker
from six import integer_types


def depends(*names):
    """
    Mark that given function depends on some other values.
    It should accept these values as kwargs,
    they will be calculated and passed by `get_caching`.
    """
    def decorator(fn):
        if not getattr(fn, 'seed_with', None):
            raise ValueError(
                'Field with dependencies should have seed field(s) declared')
        fn.depends = names
        return fn
    return decorator


def seed_with(*keys):
    """
    Mark given function to be seeded with the given keys
    rather than its field's original value.
    First matching key will be used;
    if no matching keys can be found then an error will be raised.
    If there are several matching keys
    then an error or warning should be raised;
    that is because for such fields we should either specify
    the only relevant key
    or use seed-with-original approach.
    """
    def decorator(fn):
        fn.seed_with = keys
        return fn
    return decorator


def use_original(fn):
    """
    Mark that given function needs to know the original value.
    It will be passed as `original` kwarg.
    """
    fn.use_original = True
    return fn


# Any key's value can be used, but Customer key has higher priority.
# That is primarily for Uff_Relationships.txt table
# where there are both Customer and Account keys
# but data fields are related to customer.
# Anyway, if there are several keys then the user should be warned.
kasasakey_seeds = ('customer_kasasakey', 'account_kasasakey', 'kasasakey')
seed_with_kasasakey = seed_with(*kasasakey_seeds)


# Actually we could use Faker's built in Providers system,
# but it seemingly doesn't allow any correlation
# between different columns for single record.
# We could add such correlation as a wrapper class
# but then the other problem is that we need methods from different providers
# while each provider is usually isolated.
# Hence use this way.
class KasasaFaker(object):
    """
    This class "inherits" Faker's methods
    and adds some Kasasa-specific ones,
    including those which should depend on other fields.
    Some of these methods are decorated
    to mark dependencies and keying.

    This class does not contain any logic related to field inheritance.
    """
    def __init__(self, faker=None, **kw):
        """
        :param faker: pre-created faker instance to use;
        by default will create new faker instance and pass it kwargs.

        Note that creating faker is somewhat expensive in terms of cpu time,
        so you may want to re-use single faker.
        (Consider thread-safety if applicable.)
        """
        if faker:
            self._faker = faker
        else:
            self._faker = Faker(**kw)

    def __getattr__(self, name):
        """
        Proxy all undefiend methods to faker instance
        """
        return getattr(self._faker, name)

    def seed(self, value):
        # python2's RNG should not be seeded with non-integer:
        # if you seed it with integer then it uses it properly
        # but all other values are converted to hash()
        # which is not guaranteed to be deterministic
        assert isinstance(value, integer_types)
        self._faker.seed_instance(value)

    def empty(self):
        # just an empty string
        return ''

    @seed_with_kasasakey
    def gender(self):
        return self.random_element(['male', 'female'])

    @depends('gender')
    @seed_with_kasasakey
    def first_name(self, gender):
        return getattr(self._faker, 'first_name_%s' % gender)()

    @depends('gender')
    @seed_with_kasasakey
    def last_name(self, gender):
        return getattr(self._faker, 'last_name_%s' % gender)()

    @depends('first_name', 'last_name', 'gender')
    @seed_with_kasasakey
    def full_name(self, first_name, last_name, gender):
        return ' '.join([
            first_name,
            self.first_name(gender),  # middle name
            last_name,
        ])

    @seed_with_kasasakey
    def address_line_1(self):
        # Second line of self.address() is city and state and country,
        # not actual "address line 2".
        # So we just take first line of an address
        return self.address().splitlines()[0]

    @depends('address_line_1', 'city', 'state_abbr', 'postcode', 'country')
    @seed_with_kasasakey
    def address_complete(self, address_line_1, city,
                         state_abbr, postcode, country):
        return ' '.join([
            address_line_1,
            city, state_abbr, postcode, country,
        ])

    @depends('full_name', 'address_complete')
    @seed_with_kasasakey
    def name_and_address(self, full_name, address_complete):
        return ', '.join([full_name, address_complete])

    def tax_id(self):
        return self.ssn().replace('-', '')

    def birth_date(self):
        # never use _this_year, _this_month etc
        # because they will break data integrity
        return str(
            self.date_between(dt.date(1950, 1, 1), dt.date(2005, 12, 31)))

    def customer_since_date(self):
        return str(
            self.date_between(dt.date(2000, 1, 1), dt.date(2017, 11, 1)))

    def file_creation_date_time(self):
        return str(self.date_time_between_dates(
            dt.date(2000, 1, 1), dt.date(2017, 11, 1)))

    def core_customer_number(self):
        return self.random_int(1, 100000000)

    def kasasakey(self):
        return self.uuid4()

    def core_account_number(self):
        return self.random_int(1, 100000000)

    @use_original
    def email(self, original):
        # We want to retain the original domain name from email addresses.
        # Reason: some rules handle records differently based on the domain,
        # like .gov is handled specially.
        if '@' not in original:
            # no domain in the original value (strange)...
            # so let's just generate a random email
            return self._faker.email()
        username = self._faker.user_name()
        domain = original.rsplit('@', 1)[-1]
        return '@'.join([username, domain])

    # these fields' original values will not be faked
    _passthrough = [
        # table record IDs
        'uff_relationship_id',
        'uff_address_id',
        'uff_email_address_id',

        'key_type',  # customer or account
        'financial_institution_id',
        'file_creation_date_time',
        'core_processing_date',
        'core_email_address_type',  # it is likely an enumeration

        # (eg. Student Checking, Senior Citizen Checking, Reward Checking)
        # The value that distinguishes product offering for accounts
        # within the same account type group.
        'core_product_code',
    ]

    # Note that we will mutate aliased field with its alias name,
    # not function name.
    # As a consequence, alias `address_line_1` and fn `address_within_city`
    # will give different values for the same record.
    # But then `address_complete` is broken
    # because it includes `address_within_city`, not `address_line_1`.
    # So we should be careful when using aliases
    # and keep in mind that these are different fields.
    _aliases = {
        'account_kasasakey': 'kasasakey',
        'customer_kasasakey': 'kasasakey',
        'address_line_2': 'empty',
    }

    # This is instead of seed_with decorator
    # for methods which we inherit from Faker.
    # Why do we require that? Because it should be implicit
    # and there should be no probability that e.g. "city" field
    # will be seeded with `key` when encountered as a dependency
    # but seed with its original value when encountered alone.
    # Such situation would happen if we would infer dependency seed
    # from the field it depends on.
    _seeding = {
        'country': kasasakey_seeds,
        'state_abbr': kasasakey_seeds,
        'city': kasasakey_seeds,
        'postcode': kasasakey_seeds,
    }

    def get(self, name):
        """
        Obtain factory function, seeding and dependency info
        for given field name.
        """
        # resolve alias
        if name in self._aliases:
            name = self._aliases[name]
        # obtain function (and raise KeyError if not found)
        fn = getattr(self, name)
        # obtain dependencies
        depends = getattr(fn, 'depends', ())
        seed_with = getattr(fn, 'seed_with', None)
        if not seed_with:
            # check alternate source
            seed_with = self._seeding.get(name)
        use_original = getattr(fn, 'use_original', False)
        return fn, depends, seed_with, use_original


class RecordFaker(object):
    """
    This class is responsible for everything related to dependencies,
    seeding and keying.
    Also for convenience it exposes underlying KasasaFaker's methods
    which should probably be dropped in the future.
    """
    mutation_cache = {}

    def __init__(self, original, salt, faker=None, **kw):
        """
        :param original: original field values as a dict
        :param salt: the salt value to use
        :param faker: pre-created faker instance to use;
        by default will create new faker instance and pass it kwargs.

        Note that creating faker is somewhat expensive in terms of cpu time,
        so you may want to re-use single faker.
        (Consider thread-safety if applicable.)
        """
        # all field names are expected to be lowercase;
        # let's convert them if they are not
        self.original = {
            # If we are processing SQL query result
            # then original might be non-string;
            # we want to convert it to string anyway.
            # Also we want to convert None (database's NULL) to empty strings,
            # not to "None" sting.
            k.lower(): str(v) if v is not None else ''
            for k, v in original.items()
        }
        self.salt = salt

        if isinstance(faker, KasasaFaker):
            self.faker = faker
        else:
            self.faker = KasasaFaker(faker=faker, **kw)

        # Store fields calculated so far.
        # This is required for dependency resolution.
        self.cache = {}
        # cache already calculated base seed values
        self.base_seeds = {}

    def format_field(self, field):
        """
        Calculate value for given field name.
        Will handle salting but not caching.
        """
        # We don't check for original value emptiness.
        # That check is done in format_all() method.
        # See explanation in the comment for commit #fae2846

        # is it a pass-through field?
        if field in self.faker._passthrough:
            return self.original[field]

        fn, dependency_names, seed_options, use_orig = self.faker.get(field)

        # handle dependencies (before seeding ourselves)
        resolved_deps = {
            # note this will result in infinite recursion
            # if dependencies are cyclic
            dep: self.format_field_cached(dep)
            for dep in (dependency_names or ())
        }

        if use_orig:
            resolved_deps['original'] = self.original[field]

        # which seed shall we use?
        if seed_options is not None:
            # find out which field should be used as a seed source
            matching = set(seed_options).intersection(self.original.keys())
            if not matching:
                raise ValueError(
                    'This record has no key fields to seed {!r}. '
                    'Good ones would be {}'.format(field, seed_options))
            if len(matching) > 1:
                raise ValueError(
                    'Too many matching keys for field {!r}; '
                    'cannot choose from {}'.format(field, matching))

            # find out which field should we finally use for seeding
            seed_source = next(iter(matching))
        else:
            # use our original value for seeding
            # FIXME: what if original is empty?
            # for now just raise an exception
            if self.original[field].strip() == '':
                raise ValueError(
                    'Original value for %s is empty, cannot seed it!' % field)
            seed_source = field

        # calculate seed from given field's original value
        # but mutate it with our field's mutation
        seed = self.calculate_seed_for_field(seed_source, field)

        # now seed our RNG with the seed value
        self.faker.seed(seed)
        # and generate fake value
        return fn(**resolved_deps)

    def format_field_cached(self, field):
        """
        Cache-aware wrapper around format_field
        """
        if field in self.cache:
            return self.cache[field]
        val = self.format_field(field)
        self.cache[field] = val
        return val

    def calculate_seed_for_field(self, source, target):
        """
        :param source: name of the field whose original value will be used
        :param target: name of the field which will use the seed;
        seed will be mutated with that name.
        """
        # obtain base seed (it might have been cached)
        base_seed = self.get_base_seed(source)
        # find out mutation
        mutation = self.get_mutation(target)
        # apply mutation
        return base_seed + mutation

    def get_base_seed(self, source):
        """
        Obtain base seed for given field.
        That field should be present in the original record.
        Calculation algorithm is as follows:
        1. Generalize the value
        2. Append salt
        3. Calculate SHA256 hash from the result
        4. Convert that hash to large int
        Base seed is cached.
        """
        if source in self.base_seeds:
            return self.base_seeds[source]

        # find value
        val = self.original[source]
        # generalize it:
        # drop everything but alphanumeric chars and lowercase them;
        # that is to decrease "jitter" in spelling of the same value
        val = ''.join(c.lower() for c in val if c.isalnum())
        # add some salt
        val = self.salt + val
        # calculate hash
        hashval = hashlib.sha256(val.encode()).hexdigest()
        # and convert it to integer
        seed = int(hashval, 16)

        self.base_seeds[source] = seed
        return seed

    @classmethod
    def get_mutation(cls, target):
        """
        Calculate mutation value for given target field.
        We actually just convert field name to binary and then to int.
        Result is cached in class variable because it is constant.
        """
        if target in cls.mutation_cache:
            return cls.mutation_cache[target]
        val = int(binascii.b2a_hex(str(target).lower().encode()), 16)
        cls.mutation_cache[target] = val
        return val

    def format_all(self):
        """
        Format all fields from the original record
        and return result as a dict.
        Originally-empty fields won't be calculated directly
        and instead will be filled with empty strings for returning.
        """
        return {
            key: self.format_field_cached(key)
            # if original is non-empty then calculate, else return empty string
            # (such fields still might be calculated as dependencies)
            if self.original[key].strip() else ''
            for key in self.original.keys()
        }

    @classmethod
    def format_record(cls, record, salt):
        return cls(record, salt).format_all()
