SELECT key_type, kasasakey, first_name, last_name, full_name, company_name,
address_line_1, address_line_2, city, state, postal_code, country, single_field_name_and_address,
phone_number, financial_institution_id, file_creation_date_time, core_processing_date
FROM (SELECT key_type,
					   kasasakey,
						 first_name,
						 last_name,
						 full_name,
						 company_name,
						 address_line_1,
						 address_line_2,
						 city,
						 state,
						 postal_code,
						 country,
						 single_field_name_and_address,
						 phone_number,
						 financial_institution_id,
						 file_creation_date_time,
						 core_processing_date,
						 pg_catalog.row_number() OVER (PARTITION BY kasasakey ORDER BY core_processing_date DESC) AS rownum
	FROM ods.uff_addresses)
WHERE rownum = 1;
