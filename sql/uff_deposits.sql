SELECT account_kasasakey, financial_institution_id, core_processing_date, core_account_type, open_date, close_date, branch
FROM (SELECT account_kasasakey,
             financial_institution_id,
             core_processing_date,
             core_account_type,
             open_date, close_date,
             branch,
            pg_catalog.row_number() OVER (PARTITION BY account_kasasakey ORDER BY core_processing_date DESC) AS rownum
        FROM ods.uff_deposits
     )
WHERE rownum = 1;
