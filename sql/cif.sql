-- Selects the Last Processed UFF_CIF File
with uff_latest_core_processing_date_customers as
(
       SELECT max(core_processing_date) as last_core_process, financial_institution_id
         FROM ods.uff_cif
       	GROUP BY financial_institution_id
),
-- Selects the necessary UFF_CIF records for the last processed file
cte_cif as
(
Select
	cif.customer_kasasakey,
	cif.core_customer_number,
	cif.customer_since_date,
	cif.birth_date,
	cif.tax_id,
  cif.full_name,
	cif.first_name,
	cif.last_name,
	cif.financial_institution_id,
	cif.file_creation_date_time,
	cif.core_processing_date
FROM
	 uff_latest_core_processing_date_customers
INNER JOIN
	ods.uff_cif cif
		ON cif.financial_institution_id = uff_latest_core_processing_date_customers.financial_institution_id
			AND uff_latest_core_processing_date_customers.last_core_process = cif.core_processing_date
)

----  End Data Tables ---
----  Start Query ---

SELECT * from cte_cif;
