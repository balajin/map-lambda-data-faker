SELECT key_type, kasasakey, core_email_address_type, email_address, financial_institution_id, file_creation_date_time, core_processing_date
FROM (SELECT key_type,
						 kasasakey,
						 core_email_address_type,
						 email_address,
						 financial_institution_id,
						 file_creation_date_time,
						 core_processing_date,
             pg_catalog.row_number() OVER (PARTITION BY kasasakey ORDER BY core_processing_date DESC) AS rownum
        FROM ods.uff_email_addresses
     )
WHERE rownum = 1;

/* WIP 
;
with cte_email_addresses as
(
       SELECT max(core_processing_date) as last_core_process, financial_institution_id
         FROM ods.uff_email_addresses
       	GROUP BY financial_institution_id
)
--Selects the necessary UFF_CIF records for the last processed file
Select
	uea.key_type,
	uea.kasasakey,
	uea.core_email_address_type,
	uea.email_address,
	uea.financial_institution_id,
	uea.file_creation_date_time,
	uea.core_processing_date
FROM
	 cte_email_addresses
INNER JOIN
	ods.uff_email_addresses uea
		ON uea.financial_institution_id = cte_email_addresses.financial_institution_id
			AND cte_email_addresses.last_core_process = uea.core_processing_date
;
*/