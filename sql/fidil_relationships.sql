SELECT account_kasasakey, customer_kasasakey, kasasa_relationship_code, financial_institution_id, core_processing_date
FROM (SELECT account_kasasakey,
             customer_kasasakey,
             kasasa_relationship_code,
             financial_institution_id,
             core_processing_date,
             pg_catalog.row_number() OVER (PARTITION BY account_kasasakey ORDER BY core_processing_date DESC) AS rownum
        FROM ods.fidil_relationships
     )
WHERE rownum = 1;
