from contextlib import contextmanager, closing
import datetime as dt
import io
import json
import logging
import os
import os.path as op
import sys
import tempfile

from faker import Faker
from kasasa_lambda import get_s3, get_secrets, kms_decrypt
from kasasa_lambda import parse_db_envvars
from kasasa_lambda import xray
from kasasa_lambda.misc import DDCsvDictReader, DDCsvDictWriter
from kasasa_lambda.skel import BaseSQSFileProcessor
import psycopg2
import psycopg2.extras

import csv_fields
from record_faker import RecordFaker


logger = logging.getLogger('handler')


# patch supported services (botocore, mysql-connector...)
# to report xray subsegments
# and activate logging capture
xray.activate()


class BaseHandler(object):
    """
    Common logic for both CSV and SQL based inputs
    """
    @xray.capture
    def prepare_env(self):
        """
        Prepare common environment, like target bucket or salt.
        It is this method which will raise in case of most env errors.
        """
        self.dst_bucket = os.environ['BUCKET']
        self.s3_dst = get_s3(self.dst_bucket)
        self.dst_prefix = os.environ['PREFIX']
        delimiter = os.environ.get('DELIMITER', ',')
        if delimiter.startswith('0') and len(delimiter) > 2:
            # it is a hexadecimal/octal/binary value; decode it to character
            # Format: 0x1F for hex, 0o37 for octal, 0b11111 for binary
            delimiter = chr(int(delimiter, 0))
        logger.info('Will use %(delim)r as record delimiter for output file',
                    dict(delim=delimiter))
        # we want it to be in unicode for csv module to work properly
        if hasattr(delimiter, 'decode'):
            # py2 -> need to decode it
            delimiter = delimiter.decode()
        self.delimiter = delimiter

        # Obtain salt.
        # It is stored in secrets file and additionally encrypted with KMS.
        # Reason: we are required to rotate secrets every 90 days
        # but we don't want to change actual salt value
        # to avoid losing data integrity;
        # so we rotate KMS key (and hence encrypted value)
        # while retaining the same actual salt value.
        self.secrets = get_secrets(
            bucket=os.environ.get('SECRETS_BUCKET') or os.environ['BUCKET'],
            key='secrets/passwords.json',
        )
        salt_encrypted = str(self.secrets['salt'])  # make sure it is string
        self.salt = kms_decrypt(salt_encrypted)
        logger.info('Obtained salt')  # but never log salt itself

    def open_input(self):
        """
        This should be a context manager.
        It should open input file or database or whatever
        and return tuple of (list_of_fields, iterable_of_records).
        Each record should be a dictionary
        mapping (field_name -> value)
        where field_names should match list_of_fields.
        """
        raise NotImplementedError

    @xray.capture
    def process_records(self):
        before = dt.datetime.utcnow()

        # in py2, TemporaryFile yields "old-style" stream
        # while TextIOWrapper needs "new-style".
        # So re-open it by FD.
        with tempfile.TemporaryFile() as tf_base,\
                self.open_input() as (fields, records):
            # we won't close it directly.
            # Reason for this approach (use with() for original tempfile
            # and don't close io.open()ed file):
            # 1. If we are on Windows then we need to unlink file on close;
            # 2. We cannot control `closefd` behaviour for tempfile
            # but we can control it for io.open()ed one.
            # 3. TemporaryFile will close itself in __del__ method,
            # and we cannot easily control that. At the same time,
            # we can instruct io.open() to not close fd.
            tf = io.open(tf_base.fileno(), 'w+b', closefd=False)

            logger.info('Reading source data and converting records...')

            # We open file in binary mode (the default one)
            # because this mode is required for s3 uploading.
            # But CSV requires text mode;
            # hence wrap it with TextIOWrapper.
            # In py3 we could use write_through arument
            # but it won't work in py2, hence flush tf_txt separately.
            # newline='' means we don't want to translate newlines,
            # because csv module will handle them correctly for us.
            tf_txt = io.TextIOWrapper(tf, newline='')
            writer = DDCsvDictWriter(tf_txt, fields, delimiter=self.delimiter)

            # create faker once because it is expensive
            # (TODO maybe wrap it with KasasaFaker at this same point?)
            faker = Faker()

            n = 0
            for n, record in enumerate(records, start=1):
                rf = RecordFaker(record, self.salt, faker)
                result = rf.format_all()
                writer.writerow(result)
            tf_txt.flush()  # but don't close
            tf.flush()
            logger.info('%(count)d records converted', dict(count=n))

            after = dt.datetime.utcnow()

            tf.seek(0)
            key = op.join(self.dst_prefix, self.file_name)
            logger.info(
                'Uploading file to bucket %(bucket)s under key %(key)s...',
                dict(
                    bucket=self.dst_bucket,
                    key=key,
                ),
            )
            self.s3_dst.upload(tf, key)
            logger.info(
                'Done handling file %(name)s: '
                'start %(before)s, end %(after)s, duration %(duration)s', dict(
                    name=self.file_name,
                    before=before,
                    after=after,
                    duration=after - before,
                ),
            )

    @xray.capture
    def run(self):
        with closing(self):
            logger.info('Preparing environment')
            self.prepare_env()
            logger.info('Processing records')
            self.process_records()
            logger.info('Done')

    def close(self):
        """
        Close any open connections or whatever
        """
        pass


class S3Handler(BaseHandler):
    def use_file(self, src_bucket, src_key):
        self.s3_src = get_s3(src_bucket)
        self.src_key = src_key
        self.file_name = op.basename(src_key)
        logger.info('Will process file %(name)s', dict(name=self.file_name))

    @xray.capture
    def get_fields(self):
        """
        Return list of field names for current file.
        Might raise a ValueError if file name is unknown.
        """
        logger.info('Determining file type...')
        # determine file type by its name
        # this will raise an exception if we don't know this filetype
        name = self.file_name.lower()
        if name not in csv_fields.files:
            raise ValueError('Unknown file name %r' % self.file_name)
        fields = csv_fields.files[name]  # might raise KeyError
        self.fields = fields  # for iter_records
        return fields

    @xray.capture
    def iter_records(self, reader):
        """
        Wrapper around DictCsvReader which adds fields count validation.
        """
        for line in reader:
            # We expect each record to be correct.
            # Any validation error will abort the script.
            # This is only troe for CSV files.
            # DictReader will yield extra fields under `restkey` key
            # (default None)
            # and will fill missing field with `restval` (default None).
            # That None never occurs naturally,
            # so if it is present in either keys or values
            # then we know we are wrong.
            if None in line:
                raise ValueError(
                    'Too many fields in the file: expected {} but got {}.'
                    .format(
                        len(self.fields),
                        len(self.fields) + len(line[None]),
                    ))
            if None in line.values():
                raise ValueError(
                    'Too few fields in the file: expected {} but got {}. '
                    'Consider checking DELIMITER env variable.'.format(
                        len(self.fields),
                        len(filter(lambda v: v is not None, line.values())),
                    ))
            yield line

    @contextmanager
    @xray.capture
    def open_input(self):
        logger.info('Will use %(delim)r as record delimiter for input file',
                    dict(delim=self.delimiter))

        # determine list of fields (from filename)
        fields = self.get_fields()

        with self.s3_src.open_r(self.src_key, binary=False) as src:
            reader = DDCsvDictReader(src, fields,
                                     delimiter=self.delimiter)

            yield fields, self.iter_records(reader)


class SQLHandler(BaseHandler):
    def __init__(self, sqlfile):
        """
        :param sqlfile: name of the file under sql/ directory.
        """
        sqlpath = op.join(op.dirname(__file__), 'sql', sqlfile)
        if not op.exists(sqlpath):
            raise ValueError('No such SQL file: %s' % sqlpath)
        with open(sqlpath, 'r') as f:
            self.query = f.read()
        # name of the output file
        # TODO maybe there is some better way to choose that filename?
        # For now we just use sql file name and change suffix.
        self.file_name = op.splitext(sqlfile)[0] + '.txt'

        self.db = None  # will initialize it later

    @xray.capture
    def prepare_env(self):
        # prepare common env
        super(SQLHandler, self).prepare_env()

        # and now sql-specific env:
        # read connection details from environment...
        params = parse_db_envvars(secrets=self.secrets)
        # we want cursor records to be dicts (or dict-like)
        params['cursor_factory'] = psycopg2.extras.RealDictCursor

        printparams = params.copy()
        printparams['password'] = '*' * len(printparams.get('password') or '')
        logger.info('Using DB credentials: %s', printparams)

        # now create connection
        conn = psycopg2.connect(**params)

        # and wrap it with xray
        from aws_xray_sdk.ext.dbapi2 import XRayTracedConn, XRayTracedCursor
        if isinstance(conn, XRayTracedConn):
            logger.warning(
                'psycopg2 is already supported by aws_xray_sdk, '
                'please remove wrapper code!'
            )
            self.db = conn
        else:
            meta = dict(
                database_type='postgresql',
                database_version=conn.server_version,
                name=params.get('host'),
                user=params.get('user'),
            )
            self.db = XRayTracedConn(conn, meta)

        if '__enter__' not in XRayTracedCursor.__dict__:  # hasattr won't work
            # monkey-patch for issue aws/aws-xray-sdk-python/pull/17
            def __enter__(self):
                val = self.__wrapped__.__enter__()
                if val is not self.__wrapped__:
                    return val
                return self
            XRayTracedCursor.__enter__ = __enter__
            XRayTracedConn.__enter__ = __enter__
        else:
            logger.warning(
                'aws_xray_sdk issue #17 fixed, please remove monkey')

    @contextmanager
    @xray.capture
    def open_input(self):
        with self.db.cursor() as cur:
            logger.info('Executing query')
            cur.execute(self.query)
            # get list of column names
            # cur.description is a tuple of 7-tuples,
            # first entry of them is name and others are metadata
            columns = tuple(ci[0] for ci in cur.description)
            # give tuple of (list_of_fields, iterable_of_records)
            # cursor will yield records as dicts when iterated over
            # (see our prepare_env for explanation)
            yield columns, cur

    def close(self):
        if self.db:
            logger.info('Closing DB connection')
            self.db.close()
        else:
            logger.warning('DB not opened, won\'t close')


class SQSS3Handler(BaseSQSFileProcessor):
    def load_env(self):
        super(SQSS3Handler, self).load_env()

        # prepare our handler class instance
        self.worker = S3Handler()
        self.worker.prepare_env()

    def process_file(self, bucket, key, timestamp):
        self.worker.use_file(bucket, key)
        self.worker.process_records()


@xray.capture
def process_s3(a, b):
    worker = S3Handler()
    worker.use_file(a, b)
    worker.run()


def main_s3_sns(event, context):
    msg = json.loads(event['Records'][0]['Sns']['Message'])
    src_bucket = msg['Records'][0]['s3']['bucket']['name']
    src_key = msg['Records'][0]['s3']['object']['key']

    return process_s3(src_bucket, src_key)


# handle MESSAGES_COUNT (default 10) messages and relaunch itself if need
main_s3_sqs = SQSS3Handler.lambda_main_one_by_one

# launch sublambda for each available message
main_s3_sqs_with_sublambdas = SQSS3Handler.lambda_main_with_sublambdas
# the sublambda which will process just single message
main_s3_sqs_sublambda = SQSS3Handler.lambda_main_sublambda


def main_sql(event, context):
    sqlfile = event['SQL']  # TODO will it work?
    SQLHandler(sqlfile).run()


def main_test(e, c):
    # basic wrapping for xray segment recording
    with xray.within_segment('main_test'):
        # here is the actual code
        bucket = 'bucket'
        key = 'Uff_Cif.txt'  # default
        if len(sys.argv) == 2:
            key = sys.argv[1]
        elif len(sys.argv) == 3:
            bucket, key = sys.argv[1:]
        elif len(sys.argv) != 1:
            raise ValueError('Unexpected args count %d' % len(sys.argv))
        # safeguard to avoid overwriting test data
        assert os.environ['PREFIX'] or os.environ['BUCKET'] != bucket
        process_s3(bucket, key)


if __name__ == '__main__':
    main_test(None, None)
