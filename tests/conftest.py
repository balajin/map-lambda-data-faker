import aws_xray_sdk.core
import moto
import pytest


pytest_plugins = [
    # we need it for moto_* fixtures
    'kasasa_lambda.tests.moto_helpers',
    'kasasa_lambda.tests.xray',
]


@pytest.fixture(autouse=True)
def _autouse_xray_segment(xray_segment):
    pass
