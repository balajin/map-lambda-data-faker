from collections import OrderedDict

import pytest
from six import integer_types

from record_faker import RecordFaker, KasasaFaker


def test_creation():
    rf = RecordFaker({'key': 'value'}, 'salt')
    # check that KasasaFaker instance was created
    assert rf.faker
    assert isinstance(rf.faker, KasasaFaker)

    # test re-using existing KasasaFaker instance
    rf2 = RecordFaker({'key': 'value'}, 'salt', faker=rf.faker)
    assert rf2.faker is rf.faker

    # test re-using plain Faker object
    rf3 = RecordFaker({'key': 'value'}, 'salt', faker=rf.faker._faker)
    assert isinstance(rf3.faker, KasasaFaker)
    assert rf3.faker is not rf.faker
    assert rf3.faker._faker is rf.faker._faker

    # test that keys are converted to lowercase
    rf4 = RecordFaker({
        'Key': 'abc',
        'TEST': 'def',
        'XyZZy_a12': '42',
        'kEY': 'abc',  # duplicate, we don't know which one wins
    }, '')
    assert rf4.original == {
        'key': 'abc',
        'test': 'def',
        'xyzzy_a12': '42',
    }


def test_format_field_cached(mocker):
    rf = RecordFaker({}, '')
    mocker.patch.object(rf, 'format_field', side_effect=range(1000))

    v1 = rf.format_field_cached('test')
    rf.format_field.assert_called_with('test')
    v2 = rf.format_field_cached('test')
    # check that cached value was used
    assert v2 is v1

    # check that different field name will use different value
    v3 = rf.format_field_cached('another')
    assert v3 is not v1


def test_get_base_seed():
    fields = {
        'hello': 'world',
        'a1': 'b123',
        'a2': 'B 12 3',
        'a3': 'b-!@#$%123}{',
        'empty1': '',  # TODO shall we handle empty value specially?
        'empty2': '  `~!@#$"',  # these should be resolved to empty as well
    }
    salt = 'salt'
    # pre-calculated base seed values;
    # they should never vary unless we change our algorithm or salt
    seeds = {
        'hello': 0xe84ac3142870113ddc6710c06f76421befc8e8ca6de64e98d2993ed8d41f4085,
        'a1': 0x4eccc3d7830ee874943d031bcc49e9f426e64e7c6eff4301bfcca024e150de02,
        'a2': 0x4eccc3d7830ee874943d031bcc49e9f426e64e7c6eff4301bfcca024e150de02,
        'a3': 0x4eccc3d7830ee874943d031bcc49e9f426e64e7c6eff4301bfcca024e150de02,
        'empty1': 0x63479ad69a090b258277ec8fba6f99419a2ffb248981510657c944ccd1148e97,
        'empty2': 0x63479ad69a090b258277ec8fba6f99419a2ffb248981510657c944ccd1148e97,
    }
    rf = RecordFaker(fields, salt)

    for field in fields:
        print('Testing seed for field %s' % field)
        seed = rf.get_base_seed(field)
        assert isinstance(seed, integer_types)
        print(hex(seed))
        assert seed == seeds[field]

    # unknown field should result in KeyError
    with pytest.raises(KeyError):
        rf.get_base_seed('unknown_field')

    # test that caching works:
    # corrupt original record...
    rf.original = {}
    # and make sure proper values are still returned
    assert rf.get_base_seed('a1') == seeds['a1']


    # check that different salt will result in different values
    rf = RecordFaker(fields, 'othersalt')
    for field in fields:
        assert rf.get_base_seed(field) != seeds[field]


def test_get_mutation():
    rf = RecordFaker({}, '')
    # in current implementation get_mutation is a classmethod
    # but it should not bother us,
    # this method should still match all requirements
    # even if it is re-implemented as an instance method.
    # Hence we test is as an instance-method

    # it should not depend on any fields being actually available

    # we run this test twice and try to clear cache afterwards;
    # that is to avoid cache influencing our results.

    m1 = rf.get_mutation('a')
    m2 = rf.get_mutation('A') # case should be ignored
    m3 = rf.get_mutation('hello')
    m4 = rf.get_mutation('hello') # same name should always yield same value
    m5 = rf.get_mutation('hel_lo')
    assert m1 == m2
    assert m1 != m3
    assert m3 == m4
    assert m4 != m5
    assert all(isinstance(m, integer_types) for m in (m1, m2, m3, m4, m5))

    if hasattr(rf, 'mutation_cache'):
        rf.mutation_cache.clear()
    else:
        import warnings
        warnings.warn('Looks like caching was changed!')

    m10 = rf.get_mutation('hello')
    assert m10 == m3


def test_calculate_seed_for_field():
    record = {'a': '1', 'b': '2'}
    rf = RecordFaker(record, 'seed')

    s1 = rf.calculate_seed_for_field('a', 'abc')
    s2 = rf.calculate_seed_for_field('a', 'def')
    s3 = rf.calculate_seed_for_field('b', 'abc')
    s4 = rf.calculate_seed_for_field('b', 'def')

    # they should all be different
    assert s1 != s2
    assert s1 != s3
    assert s1 != s4
    assert s2 != s3
    assert s2 != s4
    assert s3 != s4

    # now, test that same data yields same result
    assert rf.calculate_seed_for_field('a', 'abc') == s1
    assert rf.calculate_seed_for_field('b', 'def') == s4

    # and that different seed will change the result
    rf2 = RecordFaker(record, 'othersalt')
    assert rf2.calculate_seed_for_field('a', 'abc') != s1


def test_salt():
    def addr(salt):
        return RecordFaker(
            {'address': 'whatever'}, salt,
        ).format_field('address')
    val1a = addr('')
    val1b = addr('test')

    val2a = addr('x')
    val2b = addr('test')
    val2c = addr('whatever')

    assert val1a != val1b
    assert val2a != val2b
    assert val2a != val2c
    assert val1b == val2b


def test_empty():
    rec = {'empty': 'abc'}
    conv = RecordFaker.format_record(rec, salt='')
    assert conv == {'empty': ''}


def test_firstname_fullname():
    rec = {
        'first_name': 'john',
        'full_name': 'smith',
    }
    with pytest.raises(ValueError, match='no key fields to seed'):
        RecordFaker.format_record(rec, salt='')

    rec['kasasakey'] = '12345678'
    conv = RecordFaker.format_record(rec, salt='')
    assert conv['full_name'].startswith(conv['first_name'] + ' ')


class TestFormatField(object):
    def test_missing_key(self):
        rf = RecordFaker({}, '')
        with pytest.raises(KeyError, match='md5'):
            # it should not work.
            # md5 is a valid faker field
            # but we would try to key it with its original value
            # which is missing; hence KeyError.
            rf.format_field('md5')

        with pytest.raises(ValueError, match='kasasakey'):
            # first_name should be keyed with one of kasasakeys.
            # we will check that any matching key exists,
            # find that it doesn't and then raise ValueError.
            rf.format_field('first_name')

    def test_passthrough(self):
        # test just some of the known parameters
        # this test is to handle unlikely situation
        # when _passthrough field was abandoned and unused
        rf = RecordFaker({
            'key_type': 'Customer',
            'uff_address_id': '42',
        }, '')
        assert rf.format_field('key_type') == 'Customer'
        assert rf.format_field('uff_address_id') == '42'

    @pytest.mark.parametrize('seed', ('', 'abcde', 'whatever'))
    def test_all_passthrough(self, seed):
        # Make sure that all fields in pass-through list do pass through
        # regardless of the seed

        # for each passthrough field, set its value to next integer
        record = {
            key: str(val)
            for key, val in zip(
                KasasaFaker._passthrough,
                range(1000),
            )
        }

        rf = RecordFaker(record, seed)

        for k, v in record.items():
            assert rf.format_field(k) == v

    def test_aliases(self):
        record = {
            'Account_KasasaKey': '1',
            'customer_kasasaKey': '2',
            'kasasaKey': '3',
            'uuid4': '3',
            'address_line_1': '4',
            'address_line_2': '5',
        }
        results = {  # expected results
            # notice these urls are all different
            # (that was why I placed the all together)
            'akk-1': 'f74b18c8-fb05-3b93-77df-5c8689ece2f9',
            'ckk-2': '94658142-71b6-85f5-69d2-d16c969c7746',
            'kk-3': '12a0bd1b-e22b-8a09-22d3-bded91086b08',
            'u-3': 'f73d9f21-15ee-75cc-abb1-46ae9c380f5b',
            'akk-2': 'a218cf67-270e-8dec-37ed-418a228bcd08',
            'addr-8': '035 Penny Roads Suite 755',
        }

        rf = RecordFaker(record, '')

        val = rf.format_field('account_kasasakey')
        assert val == results['akk-1']

        # this one should be different.
        # why? It uses the same generator
        # but different "final" field name
        # and hence different mutation.
        val = rf.format_field('customer_kasasakey')
        assert val == results['ckk-2']

        val = rf.format_field('kasasakey')
        assert val == results['kk-3']

        val = rf.format_field('uuid4')
        assert val == results['u-3']

        with pytest.raises(ValueError, match='Too many matching keys'):
            # it should not work
            # becuase it cannot choose proper key in our fake record
            rf.format_field('address_line_1')

        # address_line_2 should always be empty
        val = rf.format_field('address_line_2')
        assert val == ''


        # now try with different values
        rf = RecordFaker({
            'Account_KasasaKey': '2',
            'address_line_1': '8',
            'address_line_2': '16',
        }, '')

        # this should be different from the original (because of the value)
        # and different from prev customer key (because of mutation)
        val = rf.format_field('account_kasasakey')
        assert val == results['akk-2']

        val = rf.format_field('address_line_1')
        assert val == results['addr-8']

        # and this one should still be empty
        val = rf.format_field('address_line_2')
        assert val == ''

    def test_semi_real_record_processing(self):
        orig = OrderedDict([
            ('Customer_KasasaKey', '11111111-1111-1111-1111-111111111111'),
            ('Core_Customer_Number', '123456'),
            ('Customer_Since_Date', '2010-12-01'),
            ('birth_date', '2011-12-01'),
            ('tax_id', '123456789'),
            ('full_name', 'John J. Smith'),
            ('first_name', 'John'),
            ('last_name', ''),  # let's consider it is empty
            ('Financial_Institution_Id', 'fi12345'),
            ('File_Creation_Date_Time', '2012-12-01'),
            ('Core_Processing_Date', '2013-12-01'),
        ])

        rf = RecordFaker(orig, '')
        result = rf.format_all()

        # these exact results should be produced
        assert result == {
            'customer_kasasakey': 'a8fe0e03-6e5c-6543-f303-51ac19c41376',
            'core_customer_number': 90664872,
            'customer_since_date': '2007-09-16',
            'birth_date': '1978-08-31',
            'tax_id': '714837044',
            'full_name': 'Phillip Scott Patterson',
            'first_name': 'Phillip',
            'last_name': '',
            'financial_institution_id': 'fi12345',  # unchanged
            'file_creation_date_time': '2012-12-01',  # unchanged
            'core_processing_date': '2013-12-01',  # unchanged
        }

    # TODO more tests, much more!
    # test seeding logic, test complex cases
    # including some real tables maybe
