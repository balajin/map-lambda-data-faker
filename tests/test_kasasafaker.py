import pytest

from record_faker import KasasaFaker


# actually not much to test here
# because that class holds almost no logic

def test_create():
    kf = KasasaFaker()
    # check that faker was properly assigned
    # i.e. that properties are properly passed through
    assert hasattr(kf, 'year')
    assert hasattr(kf, 'catch_phrase')

    # check that we can pass in a pre-created (custom) faker instance
    fakefaker = lambda: None
    fakefaker.answer = lambda: 42
    kf = KasasaFaker(fakefaker)
    assert kf.answer() == 42


def test_get():
    kf = KasasaFaker()

    f, d, s, o = kf.get('year')
    year = f()
    assert isinstance(year, str)
    assert len(year) == 4
    assert year.isdigit()
    # no dependencies expected
    assert not d
    assert len(d) == 0
    # and nothing to seed with
    assert s is None

    # now check some "well-known" stuff like full_name
    f, d, s, o = kf.get('full_name')
    # it should accept its dependencies as arguments
    fullname = f('John', 'Smith', 'male')
    assert fullname.startswith('John ')
    assert fullname.endswith(' Smith')
    assert fullname != 'John Smith'
    # dependencies should be declared
    assert set(d) == {'first_name', 'last_name', 'gender'}
    # and they should not be a one-time iterator
    assert set(d) == {'first_name', 'last_name', 'gender'}
    # seed should also be declared
    assert s
    assert 'kasasakey' in s
    assert 'customer_kasasakey' in s

    # test use_original
    f, d, s, o = kf.get('email')
    assert f('john@smith.com').endswith('@smith.com')
    # for invalid data it should genreate fully new email address
    assert 'abcde' not in f('abcde')
    assert not d
    assert s is None
    assert o is True


def test_seed():
    kf = KasasaFaker()
    # we should have access to underlying faker
    assert kf._faker
    kf.seed(0)
    # this should always yield this constant value when seeded with zero
    assert kf.random_int() == 8444
