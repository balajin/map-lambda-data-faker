import datetime as dt

import pytest
import py.path

import handler


pytest_plugins = 'kasasa_lambda.tests.dbfixture'


def test_param_parsing(mocker):
    mocker.patch.object(handler, 'process_s3')

    with pytest.raises(TypeError):
        # event is parsed, so None won't work
        handler.main_s3_sns(None, None)

    with pytest.raises(ValueError):
        # msg should be valid json
        handler.main_s3_sns({'Records': [{'Sns': {'Message': 'abcde'}}]}, None)

    assert handler.process_s3.call_count == 0

    msg = """
        {"Records": [{"s3": {
            "bucket": {"name": "bucket123"},
            "object": {"key": "path/to/file123.txt"}
        }}]}
    """
    handler.main_s3_sns({'Records': [{'Sns': {'Message': msg}}]}, None)

    handler.process_s3.assert_called_with(
        # src_bucket
        'bucket123',
        # src_key
        'path/to/file123.txt',
    )


@pytest.fixture
def env(monkeypatch):
    def patchenv(**kwargs):
        for key, val in kwargs.items():
            monkeypatch.setenv(key, val)
    return patchenv


@pytest.fixture
def fake_kms(mocker):
    return mocker.patch('kasasa_lambda.kms.KMS.decrypt')


def test_process_s3(tmpdir, env, fake_kms):
    env(
        BUCKET=tmpdir.strpath,
        PREFIX='output',
        DELIMITER='|',
    )
    # input data
    tmpdir.join('Uff_Cif.txt').write('\n'.join([
        '11111111-1111-1111-1111-111111111111|123|2017-01-01|2017-02-02|111111111|John Smith|||fi123|2017-03-03|2017-04-04',
        '22222222-2222-2222-2222-222222222222|124|2016-01-01|2016-02-02|222222222|Bill Smith|Bill|Smith|fi124|2016-03-03|2016-04-04',
    ]))
    # secrets
    tmpdir.join('secrets/passwords.json').write(
        '{"salt":"hello world"}', ensure=True)
    # here we mock out KMS as we assume it is already tested
    # (which is probably not a case yet)
    fake_kms.return_value = 'top_secret'

    handler.process_s3(tmpdir.strpath, 'Uff_Cif.txt')

    # check resulting state
    fake_kms.assert_called_with('hello world')

    out = tmpdir.join('output/Uff_Cif.txt')
    assert out.exists()
    # out.readlines() is buggy (it converts newlines), so use different way
    lines = out.read().splitlines(True)  # keepends=True
    assert len(lines) == 2
    line = lines[0]
    # private info should be replaced
    assert '111111111' not in line
    assert '111111-1111-1111-1111-111111111111' not in line
    assert 'John Smith' not in line
    assert '2017-02-02' not in line
    # output separator should be the same as for input
    assert '|' in line
    # two fields were empty, they should still be empty
    assert '|||' in line
    # check newline - it should be dos style, which is default for csv
    assert line[-2:] == '\r\n'


@pytest.fixture
def simple_csv(monkeypatch):
    import csv_fields
    monkeypatch.setitem(csv_fields.files, 'test.txt', [
        'customer_kasasakey',
        'email',
        'first_name',
        'full_name',
    ])


def test_process_s3_mocker(moto_s3, simple_csv, env, fake_kms):
    """
    Simplified version of test_process_s3
    but using "semi-real" bucket instead of directory-based simulation.
    It is a bit harder to configure but might catch some interface errors
    not noticed by other tests
    """

    # create bucket
    bucket = moto_s3.Bucket('test-bucket')
    bucket.create()

    env(
        BUCKET=bucket.name,
        PREFIX='output',
    )

    # create input data
    bucket.put_object(Key='UFF_CIF.txt', Body=b'\n'.join([
        b'11111111-1111-1111-1111-111111111111,123,2017-01-01,2017-02-02,111111111,John Smith,,,fi123,2017-03-03,2017-04-04',
        b'22222222-2222-2222-2222-222222222222,124,2016-01-01,2016-02-02,222222222,Bill Smith,Bill,Smith,fi124,2016-03-03,2016-04-04',
    ]))

    # and secrets
    bucket.put_object(
        Key='secrets/passwords.json',
        Body=b'{"salt":"hello world"}',
    )
    # that secret salt will be "decrypted" to -
    fake_kms.return_value = 'top_secret'

    # now do the stuff
    handler.process_s3(bucket.name, 'UFF_CIF.txt')

    # quickly test results
    out = bucket.Object('output/UFF_CIF.txt').get()['Body'].read()
    lines = out.splitlines(True)  # keepends
    assert len(lines) == 2
    line = lines[0]
    # private info should be replaced
    assert '111111111' not in line
    assert '111111-1111-1111-1111-111111111111' not in line
    assert 'John Smith' not in line
    assert '2017-02-02' not in line
    # output separator should be the same as for input
    assert ',' in line
    # two fields were empty, they should still be empty
    assert ',,,' in line
    # check newline - it should be dos style, which is default for csv
    assert line[-2:] == '\r\n'


@pytest.mark.parametrize('delimiter', ('0x1c', '0X1C', '0x1C'))
def test_hex_delimiter(simple_csv, tmpdir, fake_kms, env, delimiter):
    tmpdir.join('test.txt').write(
        '123456\x1Cjohn@smith.com\x1CJohn\x1CJohn J. Smith')
    tmpdir.join('secrets/passwords.json').write(
        '{"salt":"hello world"}', ensure=True)
    fake_kms.return_value = 'top_secret'
    env(
        BUCKET=tmpdir.strpath,
        PREFIX='output',
        DELIMITER=delimiter,
    )

    handler.process_s3(tmpdir.strpath, 'test.txt')

    out = tmpdir.join('output', 'test.txt')
    assert out.exists()
    assert out.read() == (
        '8a85d78f-48bb-f3ff-110e-b0835ab62082\x1c'
        'patrick38@smith.com\x1c'
        'Stephanie\x1c'
        'Stephanie Lisa Grant\r\n'
    )


def test_unknown_filename(env, tmpdir, fake_kms):
    env(
        BUCKET=tmpdir.strpath,
        PREFIX='output',
    )
    with pytest.raises(ValueError, match='Unknown file name'):
        handler.process_s3(tmpdir.strpath, 'unknown.txt')

    # but known files should be properly handled regardless of case
    # (it will fail on a later stage)
    with pytest.raises(IOError, match='No such file or directory'):
        handler.process_s3(tmpdir.strpath, 'uFF_cIF.tXt')


def test_record_count_validation(env, tmpdir, fake_kms, simple_csv):
    env(
        BUCKET=tmpdir.strpath,
        PREFIX='output',
        # delimiter is default comma
    )
    fake_kms.return_value = ''
    infile = tmpdir.join('test.txt')

    infile.write('a,b,c,d,e')
    with pytest.raises(ValueError, match='Too many fields.*'
                       'expected 4 but got 5'):
        handler.process_s3(tmpdir.strpath, 'test.txt')

    # first line was okay but second one is failing
    infile.write('a,b,c,d\nx,y,z')
    with pytest.raises(ValueError, match='Too few fields.*'
                       'expected 4 but got 3'):
        handler.process_s3(tmpdir.strpath, 'test.txt')

    assert not tmpdir.join('output', 'test.txt').exists()


csvs_dir = py.path.local(__file__).join('..', 'csvs')
csv_files = csvs_dir.listdir(lambda f: f.ext in ('.csv', '.txt'))
@pytest.mark.parametrize('csvfile', csv_files)
def test_various_files(csvfile, env, tmpdir, fake_kms):
    expected = csvfile.join('..', 'output', csvfile.basename)

    env(
        BUCKET=tmpdir.strpath,
        PREFIX='',
        DELIMITER='|',
    )
    fake_kms.return_value = ''
    tmpdir.join('secrets/passwords.json').write(
        '{"salt":"hello world"}', ensure=True)

    handler.process_s3(csvfile.dirname, csvfile.basename)

    out = tmpdir.join(csvfile.basename)
    assert out.exists()
    # test it only now so that if it was not supplied then
    # already-generated version will be available in tmpdir for reference
    assert expected.exists()
    assert out.read() == expected.read()


class TestSqlHandler(object):
    @pytest.fixture
    def db(self, db, mocker):
        """
        Override our common db fixture to handle psycopg2 connections
        """
        make_connection = db.connect.side_effect
        conn = mocker.patch('psycopg2.connect', side_effect=make_connection)
        db.connect = conn
        # don't validate args as it is much harder for psycopg2
        db.real_init = lambda *a, **kw: None
        return db

    def test_missing_file(self):
        with pytest.raises(ValueError, match='No such.* file'):
            handler.SQLHandler('notexisting.sql')

    def test_basic(self, db, env, tmpdir, fake_kms):
        env(
            BUCKET=tmpdir.strpath,
            PREFIX='out',
            DATABASE_URL='pg://user@hostname/mydb',
            DATABASE_PASSWORD_KEY='psql_password',
        )
        tmpdir.join('secrets', 'passwords.json').write(
            '{"salt": "", "psql_password": "thepass"}', ensure=True)
        fake_kms.return_value = ''  # salt

        rest = (None,) * 6
        rec = (
            ('customer_kasasakey', '12345678-1234-1234-1234-132435465768'),
            ('core_customer_number', 12345),
            ('customer_since_date', dt.date(2010, 1, 2)),
            ('birth_date', dt.date(1991, 1, 5)),
            ('tax_id', '123234345'),
            ('full_name', 'John J. Smith'),
            ('first_name', 'John'),
            ('last_name', None),  # if field is NULL then we'll get None
            ('financial_institution_id', 42),
            ('file_creation_date_time', dt.date(2016, 1, 5)),
            ('core_processing_date', dt.date(2016, 2, 1)),
        )
        db.on(
            'select.*customer_kasasakey,.*'
            'from.*uff_latest_core_processing_date_customers',
            [dict(rec)],
            (  # fields description
                ((key,) + (None,)*6)
                for key, val in rec
            ),

        )

        h = handler.SQLHandler('cif.sql')
        h.run()

        outfile = tmpdir.join('out', 'cif.txt')
        assert outfile.exists()
        assert outfile.read() == (
            '6adf36ad-a216-8907-9ad9-ab6363676c5b,23130458,'
            '2008-08-10,1972-08-16,518069878,'
            'Mark Matthew Arroyo,Mark,,'
            '42,2016-01-05,2016-02-01\r\n'
        )
