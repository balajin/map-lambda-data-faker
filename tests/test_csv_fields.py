# there is no code in that module, just data -
# let's check data integrity

import csv_fields
from record_faker import KasasaFaker


def test_data_integrity():
    assert isinstance(csv_fields.files, dict)
    kf = KasasaFaker()  # for checks

    for name, fields in csv_fields.files.items():
        assert isinstance(name, str)
        # file- and field names are expected to be all-lowercase
        assert not any(c.isupper() for c in name)

        assert isinstance(fields, list)
        # there should be no duplicating fields
        assert len(fields) == len(set(fields))
        for field in fields:
            assert isinstance(field, str)
            # field names should not contain uppercase
            assert not any(c.isupper() for c in field)
            # and they should be known to KasasaFaker
            if field not in kf._passthrough:
                assert kf.get(field)
