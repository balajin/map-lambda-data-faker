# This class maps CSV filenames
# to field name lists.
# Fields count should match.

files = {
    'uff_addresses.txt': [
        'uff_address_id',
        'key_type',
        'kasasakey',
        'first_name',
        'last_name',
        'full_name',
        'company',
        'address_line_1',
        'address_line_2',  # always empty in our implementation
        'city',
        'state_abbr',
        'postcode',
        'country',
        'name_and_address',
        'phone_number',
        'financial_institution_id',
        'file_creation_date_time',
        'core_processing_date',
    ],
    'uff_cif.txt': [
        'customer_kasasakey',
        'core_customer_number',
        'customer_since_date',
        'birth_date',
        'tax_id',
        'full_name',
        'first_name',
        'last_name',
        'financial_institution_id',
        'file_creation_date_time',
        'core_processing_date',
    ],
    'uff_email_addresses.txt': [
        'key_type',
        'kasasakey',
        'core_email_address_type',
        'email',
        'financial_institution_id',
        'file_creation_date_time',
        'core_processing_date',
    ],
}
